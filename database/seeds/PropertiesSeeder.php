<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Str;

class PropertiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collection = (new FastExcel())->import('./database/data.xlsx');
        foreach($collection as $entry) {
            DB::table('properties')->insert([
                'guid' => Str::uuid(),
                'suburb' => $entry['Suburb'],
                'state' => $entry['State'],
                'country' => $entry['Country'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
