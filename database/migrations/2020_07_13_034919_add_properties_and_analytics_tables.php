<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPropertiesAndAnalyticsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('guid', 36)->unique();
            $table->string('suburb');
            $table->string('state');
            $table->string('country');
            $table->timestamps();
        });

        Schema::create('analytic_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('units');
            $table->boolean('is_numeric');
            $table->integer('num_decimal_places');
            $table->timestamps();
        });

        Schema::create('property_analytics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('analytic_type_id');
            $table->text('value');
            $table->timestamps();

            $table->foreign('property_id')->references('id')->on('properties');
            $table->foreign('analytic_type_id')->references('id')->on('analytic_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_analytics');
        Schema::dropIfExists('analytic_types');
        Schema::dropIfExists('properties');
    }
}
