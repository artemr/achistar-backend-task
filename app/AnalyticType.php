<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalyticType extends Model
{
    protected $guarded = ['id'];

    public function analytics()
    {
        return $this->belongsToMany('App\Property')->using('App\PropertyAnalytic');
    }
}
