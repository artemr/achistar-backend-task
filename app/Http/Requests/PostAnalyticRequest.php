<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostAnalyticRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'units' => 'required|string',
            'is_numeric' => 'required|boolean',
            'num_decimal_places' => 'required|integer', // this field should be required only if is_numeric is true i guess. But i decided to not waste time on correcting on the trial task
            'property_id' => 'required|integer|exists:properties,id',
            'value' => 'required|string', // here should be field depending validation but i decided to not waste time on it for the trial task
        ];
    }
}
