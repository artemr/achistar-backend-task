<?php

namespace App\Http\Controllers;

use App\AnalyticType;
use App\Http\Requests\AddPropertyRequest;
use App\Http\Requests\PostAnalyticRequest;
use App\Property;
use App\PropertyAnalytic;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class PropertiesController
 * @package App\Http\Controllers
 */
class PropertiesController extends Controller
{
    /**
     * Add new property
     *
     * @param AddPropertyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addProperty(AddPropertyRequest $request) {
        $data = $request->all();
        $data['guid'] = Str::uuid();
        $model = Property::create($data);
        return response()->json(['success' => true, 'data' => $model]);
    }

    /**
     * Stores or updates analytics by provided params
     *
     * @param PostAnalyticRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAnalytics(PostAnalyticRequest $request) {
        $data = $request->all();
        $analyticType = AnalyticType::firstOrCreate([
            'name' => $data['name'],
            'units' => $data['units'],
            'is_numeric' => $data['is_numeric'],
            'num_decimal_places' => $data['num_decimal_places']
        ]);
        $analytic = PropertyAnalytic::updateOrCreate([
            'property_id' => $data['property_id'],
            'analytic_type_id' => $analyticType->id
        ], ['value' => $data['value']]);

        return response()->json(['success' => true, 'data' => compact('analyticType', 'analytic')]);
    }

    /**
     * Returns analytics for provided property
     *
     * @param Property $property
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPropertyAnalytics(Property $property) {
        $analytics = $property->analytics()->get();
        return response()->json(['success' => true, 'data' => $analytics]);
    }

    /**
     * Returns summary for properties selected by provided field ($type) and field value ($value)
     *
     * @param string $type
     * @param string $value
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSummary(string $type, string $value) {
        switch ($type) {
            case 'suburb':
                $properties = Property::where('suburb', $value)->with('analytics')->get();
                $summary = $this->getPropertiesSummary($properties);
                break;
            case 'state':
                $properties = Property::where('state', $value)->with('analytics')->get();
                $summary = $this->getPropertiesSummary($properties);
                break;
            case 'country':
                $properties = Property::where('country', $value)->with('analytics')->get();
                $summary = $this->getPropertiesSummary($properties);
                break;
            default:
                $summary = [];
        }

        return response()->json(['success' => true, 'data' => $summary]);
    }

    /**
     * Properties handler to calculate summary array
     *
     * @param Collection $properties
     * @return array
     */
    private function getPropertiesSummary(Collection $properties) {
        $summary = [];
        foreach ($properties as $prop) {
            $this->calcPropSummary($prop, $summary);
        }
        $this->calcAvgSummaryValues($summary, count($properties));

        return $summary;
    }

    /**
     * Calculates summary for the given property
     *
     * @param Property $prop
     * @param array $summary
     */
    private function calcPropSummary(Property $prop, array &$summary) {
        foreach ($prop->analytics as $entry) {
            if(isset($summary[$entry->name])) {
                if($entry->is_numeric) {
                    $value = (int) $entry->pivot->value;
                    $summary[$entry->name]['min_value'] = ($value < $summary[$entry->name]['min_value']) ? $value : $summary[$entry->name]['min_value'];
                    $summary[$entry->name]['max_value'] = ($value > $summary[$entry->name]['max_value']) ? $value : $summary[$entry->name]['max_value'];
                    $summary[$entry->name]['median_values'][] = $value;
                }
                $summary[$entry->name]['props_count']++;
            } else {
                $summary[$entry->name] = [
                    'props_count' => 1
                ];
                if($entry->is_numeric) {
                    $value = (int) $entry->pivot->value;
                    $summary[$entry->name] = array_merge([
                        'min_value' => $value,
                        'max_value' => $value,
                        'median_values' => [$value]
                    ], $summary[$entry->name]);
                }
            }
        }
    }

    /**
     * Calculates averages values for provided summary array
     *
     * @param array $summary
     * @param integer $propsCount
     */
    private function calcAvgSummaryValues(array &$summary, int $propsCount) {
        foreach ($summary as &$entry) {
            if(isset($entry['median_values'])) {
                $entry['median_value'] = array_sum($entry['median_values'])/count($entry['median_values']);
            }
            $entry['filled_percentage'] = round($entry['props_count']/$propsCount*100, 2);
            $entry['not_filled_percentage'] = 100 - $entry['filled_percentage'];
        }
    }
}
