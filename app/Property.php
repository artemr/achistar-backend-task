<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $guarded = ['id'];

    public function analytics()
    {
        return $this->belongsToMany('App\AnalyticType', 'property_analytics')->withPivot('value');
    }
}
