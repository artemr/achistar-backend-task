<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyAnalytic extends Model
{
    protected $guarded = ['id'];
    public $incrementing = true;

    public function property()
    {
        return $this->belongsTo('App\Property');
    }

    public function analytic()
    {
        return $this->belongsTo('App\AnalyticType');
    }
}
