## Deployment
- Clone the repo
- Run composer from the repo's folder

```
composer install
```
- Update .env file with DB configurations
- Run migrations

```
php artisan migrate
```
- Run seeder
```
php artisan db:seed
```
- Run local development server
```
php artisan serve
```
- Import collection to postman: postman_collection.json