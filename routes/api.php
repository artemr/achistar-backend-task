<?php

use Illuminate\Support\Facades\Route;

Route::put('/properties', 'PropertiesController@addProperty');
Route::post('/analytics', 'PropertiesController@postAnalytics');
Route::get('/properties/{property}', 'PropertiesController@getPropertyAnalytics');
Route::get('/summary/{type}/{value}', 'PropertiesController@getSummary');
